﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SimpleLibrarySE.Startup))]
namespace SimpleLibrarySE
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

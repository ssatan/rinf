﻿using SimpleLibrarySE.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SimpleLibrarySE.Models
{
    public partial class LibraryContext : DbContext
    {
        //enables CRUD functionality
        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Category> Categories { get; set; }

        public LibraryContext()
            : base("name=LibraryContext")  // base("name=DefaultConnection")
        {
            Database.SetInitializer(new LibraryInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Here is nothing to do!
            return;
            /*
            modelBuilder.Entity<Author>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Author>()
                .Property(e => e.Surname)
                .IsUnicode(false);

            modelBuilder.Entity<Author>()
                .Property(e => e.Nick)
                .IsUnicode(false);
            /*
            modelBuilder.Entity<Author>()
                .HasMany(e => e.Writers)
                .WithRequired(e => e.Author)
                .WillCascadeOnDelete(false);
            */
            /*
            modelBuilder.Entity<Author>()
                .HasMany(e => e.Books)
                .WithMany(e => e.Authors)
                .Map(m => m.ToTable("BooksAuthors").MapLeftKey("AuthorID").MapRightKey("BookID"));

            modelBuilder.Entity<Book>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Book>()
                .Property(e => e.Publisher)
                .IsUnicode(false);

            modelBuilder.Entity<Book>()
                .Property(e => e.ShortDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Book>()
                .Property(e => e.Cover)
                .IsUnicode(false);

            modelBuilder.Entity<Book>()
                .Property(e => e.Textpart)
                .IsUnicode(false);
            /*
            modelBuilder.Entity<Book>()
                .HasMany(e => e.Writers)
                .WithRequired(e => e.Book)
                .WillCascadeOnDelete(false);
            */
            /*
            modelBuilder.Entity<Book>()
                .HasMany(e => e.Categories)
                .WithMany(e => e.Books)
                .Map(m => m.ToTable("CatBooks").MapLeftKey("BookID").MapRightKey("CatID"));

            modelBuilder.Entity<Category>()
                .Property(e => e.CatDescription)
                .IsUnicode(false);
            */
        }

    }
}
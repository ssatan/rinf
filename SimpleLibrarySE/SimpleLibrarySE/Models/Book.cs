namespace SimpleLibrarySE.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Book
    {
        public Book()
        {
            //Writers = new HashSet<Writer>();
            Authors = new HashSet<Author>();
            Categories = new HashSet<Category>();
        }

        public int BookID { get; set; }

        [Required]
        [StringLength(200)]
        public string Title { get; set; }

        public int? Quantity { get; set; }

        [StringLength(100)]
        public string Publisher { get; set; }

        [StringLength(1000)]
        public string ShortDescription { get; set; }

        [StringLength(200)]
        public string Cover { get; set; }

        [StringLength(3000)]
        public string Textpart { get; set; }

        //public virtual ICollection<Writer> Writers { get; set; }

        public virtual ICollection<Author> Authors { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
    }
}

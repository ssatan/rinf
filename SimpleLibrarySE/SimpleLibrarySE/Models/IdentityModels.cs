﻿using Microsoft.AspNet.Identity;  //IUser, IRole
using Microsoft.AspNet.Identity.EntityFramework;
using System; //Guid
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity; //ICollection
using System.Threading.Tasks; //MyUsersStore

namespace SimpleLibrarySE.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.

    public class ApplicationUser : IdentityUser
	{
	    public ApplicationUser()
	    {
	        this.Id = Guid.NewGuid().ToString();
	    }
	    public ApplicationUser(string userName): this()
	    {
	        UserName = userName;
	    }
        [Key]
	    public override string Id { get; set; }
	    public override string PasswordHash { get; set; }
	    public override string SecurityStamp { get; set; }
        [Required]
        [StringLength(30)]
	    public override string UserName { get; set; }
        public ApplicationRole Role;
	}
    public class ApplicationRole : IdentityRole //IRole
    {
        public ApplicationRole()
        {
            this.Id = Guid.NewGuid().ToString(); Users = new HashSet<ApplicationUser>();
        }
        public ApplicationRole(string Name) : this() { this.Name = Name;  }
        //[Key]
        //public string Id { get; set; }
        //[Required]
        //[StringLength(30)]
        //public string Name { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }
    }

    public class MyUserStore : IUserStore<ApplicationUser>, IUserPasswordStore<ApplicationUser>, IUserSecurityStampStore<ApplicationUser>
	{
	    UserStore<IdentityUser> userStore = new UserStore<IdentityUser>(new ApplicationDbContext());
	    public MyUserStore()
	    {
	    }
	    public Task CreateAsync(ApplicationUser user)
	    {
	        var context = userStore.Context as ApplicationDbContext;
	        context.Users.Add(user);
	        context.Configuration.ValidateOnSaveEnabled = false;
	        return context.SaveChangesAsync();
	    }
	    public Task DeleteAsync(ApplicationUser user)
	    {
	        var context = userStore.Context as ApplicationDbContext;
	        context.Users.Remove(user);
	        context.Configuration.ValidateOnSaveEnabled = false;
	        return context.SaveChangesAsync();
	    }
	    public Task<ApplicationUser> FindByIdAsync(string userId)
	    {
            var context = userStore.Context as ApplicationDbContext;
	        return context.Users.Include(u => u.Id.ToLower() == userId.ToLower()).FirstOrDefaultAsync();
	    }
	    public Task<ApplicationUser> FindByNameAsync(string userName)
	    {
	        var context = userStore.Context as ApplicationDbContext;
	        return context.Users.Include(u => u.UserName.ToLower() == userName.ToLower()).FirstOrDefaultAsync();
	    }
	    public Task UpdateAsync(ApplicationUser user)
	    {
	        var context = userStore.Context as ApplicationDbContext;
	        context.Users.Attach(user);
	        context.Entry(user).State = EntityState.Modified;
	        context.Configuration.ValidateOnSaveEnabled = false;
	        return context.SaveChangesAsync();
	    }
	    public void Dispose()
	    {
	        userStore.Dispose();
	    }
	    
	    public Task<string> GetPasswordHashAsync(ApplicationUser user)
	    {
	        var identityUser = ToIdentityUser(user);
	        var task = userStore.GetPasswordHashAsync(identityUser);
	        SetApplicationUser(user, identityUser);
	        return task;
	    }
	    public Task<bool> HasPasswordAsync(ApplicationUser user)
	    {
	        var identityUser = ToIdentityUser(user);
	        var task = userStore.HasPasswordAsync(identityUser);
	        SetApplicationUser(user, identityUser);
	        return task;
	    }
	    public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash)
	    {
	        var identityUser = ToIdentityUser(user);
	        var task = userStore.SetPasswordHashAsync(identityUser, passwordHash);
	        SetApplicationUser(user, identityUser);
	        return task;
	    }
	    public Task<string> GetSecurityStampAsync(ApplicationUser user)
	    {
	        var identityUser = ToIdentityUser(user);
	        var task = userStore.GetSecurityStampAsync(identityUser);
	        SetApplicationUser(user, identityUser);
	        return task;
	    }
	    public Task SetSecurityStampAsync(ApplicationUser user, string stamp)
	    {
	        var identityUser = ToIdentityUser(user);
	        var task = userStore.SetSecurityStampAsync(identityUser, stamp);
	        SetApplicationUser(user, identityUser);
	        return task;
	    }
	    private static void SetApplicationUser(ApplicationUser user, IdentityUser identityUser)
	    {
	        user.PasswordHash = identityUser.PasswordHash;
	        user.SecurityStamp = identityUser.SecurityStamp;
	        user.Id = identityUser.Id;
	        user.UserName = identityUser.UserName;
	    }
	    private IdentityUser ToIdentityUser(ApplicationUser user)
	    {
	        return new IdentityUser
	        {
	            Id = user.Id,
	            PasswordHash = user.PasswordHash,
	            SecurityStamp = user.SecurityStamp,
	            UserName = user.UserName
	        };
	    }
	}

    /*
    public class ApplicationUser : IdentityUser
    {}
    */
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
            Database.SetInitializer(new IdentityInitializer());
        }
        //public virtual IDbSet<ApplicationUser> Users { get; set; }
    }


    public class IdentityInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext> //DropCreateDatabaseAlways<ApplicationDbContext>
    //public class IdentityInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            base.Seed(context);
            /* It is bad idea
            var rA = new ApplicationRole("Admin");
            var rU = new ApplicationRole("User");
            context.Roles.Add(rA);
            context.Roles.Add(rU);
            var uJ = new ApplicationUser("Jasmina");
            uJ.Role = rU;
            uJ.Password ash = "rinf";
            var uA = new ApplicationUser("Piotr");
            uA.PasswordHash = "rinf"; uA.Role = rA;
            context.Users.Add(uA); context.Users.Add(uJ);
            context.SaveChanges();
             */
        }
    }
}
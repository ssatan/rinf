namespace SimpleLibrarySE.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Category
    {
        public Category()
        {
            Books = new HashSet<Book>();
        }

        [Key]
        public int CatID { get; set; }

        [Required]
        [StringLength(30)]
        public string CatDescription { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}

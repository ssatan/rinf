namespace SimpleLibrarySE.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Author
    {
        public Author()
        {
            //Writers = new HashSet<Writer>();
            Books = new HashSet<Book>();
        }

        public int AuthorID { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [Required]
        [StringLength(60)]
        public string Surname { get; set; }

        [StringLength(30)]
        public string Nick { get; set; }

        //public virtual ICollection<Writer> Writers { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}

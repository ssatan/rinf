﻿using System;
using System.Linq;
using SimpleLibrarySE.Models;
using System.Collections.Generic;

namespace SimpleLibrarySE.ViewModels
    {

        public class PagingInfo
        {
            public int TotalItems { get; set; }
            public int ItemsPerPage { get; set; }
            public int CurrentPage { get; set; }

            public int TotalPages
            {
                get { return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage); }
            }
        }
        public class BooksListViewModel
        {
            public IEnumerable<Book> books { get; set; }
            public PagingInfo pagingInfo { get; set; }
        }
    }

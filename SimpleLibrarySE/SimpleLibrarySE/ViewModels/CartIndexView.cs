﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SimpleLibrarySE.Models;

namespace SimpleLibrarySE.ViewModels
{
    public class CartIndexViewModel
    {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
}
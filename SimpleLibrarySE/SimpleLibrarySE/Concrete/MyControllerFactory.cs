﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleLibrarySE.Abstract;

namespace SimpleLibrarySE.Concrete
{
    public class CustomControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
        {
            IBookRepository br=new EFBookRepository();
            IController controller = Activator.CreateInstance(controllerType, br) as Controller;
            return controller;
        }
    }
}
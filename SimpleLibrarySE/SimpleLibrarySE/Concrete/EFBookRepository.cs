﻿namespace SimpleLibrarySE.Concrete
{
    using System;
    using System.Linq;

    using SimpleLibrarySE.Abstract;
    using SimpleLibrarySE.Models;

    public class EFBookRepository : IDisposable, IBookRepository
    {
        private LibraryContext context;

        public EFBookRepository()
        {
            this.context = new LibraryContext();
        }

        public IQueryable<Book> Books { get { return context.Books; } }
        public IQueryable<Author> Authors { get { return context.Authors; } }
        public IQueryable<Category> Categories { get { return context.Categories; } }

        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }
    }
}

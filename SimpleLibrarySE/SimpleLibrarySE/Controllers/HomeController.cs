﻿using SimpleLibrarySE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleLibrarySE.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            LibraryContext db = new LibraryContext();
            //It stables model and cans create database
            var x = db.Books.ToList();
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
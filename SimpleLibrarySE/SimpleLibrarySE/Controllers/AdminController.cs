﻿using SimpleLibrarySE.Abstract;
using SimpleLibrarySE.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SimpleLibrarySE.Controllers {

    [Authorize]
    public class AdminController : Controller {
        private IBookRepository repository;

        public AdminController(IBookRepository repo) {
            repository = repo;
        }

        public ViewResult Index() {
            return View(repository.Books);
        }

        public ViewResult Edit(int BookId) {
             Book book = repository.Books
                .FirstOrDefault(b => b.BookID == BookId);
            return View(book);
        }


        public ViewResult Create() {
            return View("Edit", new Book());
        }

        /*
        [HttpPost]
        public ActionResult Delete(int BookId) {
            Book deletedBook = repository.DeleteProduct(productId);
            if (deletedBook != null) {
                TempData["message"] = string.Format("Usunięto {0}", 
                    deletedBook.Name);
            }
            return RedirectToAction("Index");
        }
        */
    }
}

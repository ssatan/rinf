﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleLibrarySE.Models;

namespace SimpleLibrarySE.Abstract
{
    public interface IBookRepository
    {
        IQueryable<Book> Books { get; }
        IQueryable<Author> Authors { get; }
        IQueryable<Category> Categories { get; }
    }
}

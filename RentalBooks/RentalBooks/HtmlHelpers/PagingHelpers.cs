﻿using System;
using System.Text;
using System.Web.Mvc;
using RentalBooks.Models;

namespace RentalBooks.HtmlHelpers
{

    public static class PagingHelpers
    {

        public static MvcHtmlString PageLinks(this HtmlHelper html,
                                              PagingInfo pagingInfo,
                                              Func<int, string> pageUrl)
        {

            StringBuilder result = new StringBuilder();
            TagBuilder tg = new TagBuilder("input");
            tg.MergeAttribute("type", "button");
            tg.MergeAttribute("value", "<");
            tg.MergeAttribute("onClick", "window.location.href='"+ pageUrl(1).ToString() + "'" );
            result.Append(tg.ToString());
            for (int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a"); // tworzenie znacznika <a>
                tag.MergeAttribute("href", pageUrl(i));
                tag.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage)
                    tag.AddCssClass("selected");
                result.Append(tag.ToString());
            }
            tg = new TagBuilder("input");
            tg.MergeAttribute("type", "button");
            tg.MergeAttribute("value", ">");
            tg.MergeAttribute("onClick", "window.location.href='" + pageUrl(pagingInfo.TotalPages).ToString() + "'");
            result.Append(tg.ToString());

            return MvcHtmlString.Create(result.ToString());
        }
    }
}
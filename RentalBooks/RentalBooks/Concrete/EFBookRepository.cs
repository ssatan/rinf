﻿namespace RentalBooks.Concrete
{
    using System;
    using System.Linq;

    using RentalBooks.Abstract;
    using RentalBooks.Models;

    public class EFBookRepository : IDisposable, IBookRepository
    {
        private LibraryContext context;

        public EFBookRepository()
        {
            this.context = new LibraryContext();
        }

        public IQueryable<Book> Books { get { return context.Books; } }
        public IQueryable<Author> Authors { get { return context.Authors; } }
        public IQueryable<Category> Categories { get { return context.Categories; } }

        public void SaveBook(Book book)
        {

            if (book.BookID == 0)
            {
                context.Books.Add(book);
            }
            else
            {
                Book dbEntry = context.Books.Find(book.BookID);
                if (dbEntry != null)
                {
                    dbEntry.Cover = book.Cover;
                    dbEntry.Publisher = book.Publisher;
                    dbEntry.Textpart = book.Textpart;
                    dbEntry.Title = book.Title;
                    dbEntry.Quantity = book.Quantity;
                }
            }
            context.SaveChanges();
        }

        public Book DeleteBook(int bookId)
        {
            Book dbEntry = context.Books.Find(bookId);
            if (dbEntry != null)
            {
                context.Books.Remove(dbEntry);
                context.SaveChanges();
            }
            return dbEntry;
        }


        public void Dispose()
        {
            if (this.context != null)
            {
                this.context.Dispose();
            }
        }
    }
}

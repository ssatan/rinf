// <auto-generated />
namespace RentalBooks.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class firstname : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(firstname));
        
        string IMigrationMetadata.Id
        {
            get { return "201409100425129_firstname"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RentalBooks.Models;
using RentalBooks.Abstract;

namespace RentalBooks.Controllers
{
    public class BookController : Controller
    {
        //private IEnumerable<Book> ListOfBooks=null;
        private IBookRepository repo;
        public int PageSize = 2;
        //
        // GET: /Book/
        private List<Book> books;
        public BookController(IBookRepository repo)
        {
            this.repo = repo; books = repo.Books.ToList();
        }
        public ViewResult Index()
        {
            return View(repo.Books);
        }
            
        public ViewResult Edit(int bookId, string rUrl)
        {
            Book bk = repo.Books.FirstOrDefault(b => b.BookID == bookId);
            return View(new ItemViewModel { book = bk, returnUrl = rUrl });
        }

        [HttpPost]
        public ActionResult Edit(Book book) //Edit(Book book, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                repo.SaveBook(book);
                return RedirectToAction("Index");
            }
            else
            {
                // problems
                return View(book);
            }
        }
        public RedirectToRouteResult Edition(int bookId, string returnUrl)
        {
            return RedirectToAction("Edit", new { bookId, rUrl=returnUrl } );
        }


        public ViewResult List(int page = 1)
        {
            //if (ListOfBooks == null) { ListOfBooks = new LibraryContext().Books.ToList(); }
            ViewBag.Title = "Lend a Book ...";
            BooksListViewModel viewModel = new BooksListViewModel
            {
                books = books.OrderBy(b => b.BookID).Skip((page - 1) * PageSize).Take(PageSize),
                pagingInfo = new PagingInfo { CurrentPage = page, ItemsPerPage = PageSize, TotalItems = repo.Books.Count() }
            };
            return View(viewModel);
        }


        //
        // GET: /Book/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /Book/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Book/Create
        //[HttpPost]
        //public ActionResult Create(FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add insert logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //
        // GET: /Book/Edit/5
        //public ActionResult Edit(int id)
        //{
        //    return View();
        //}

        //
        // POST: /Book/Edit/5
        //[HttpPost]
        //public ActionResult Edit(int id, FormCollection collection)
        //{
        //    try
        //    {
        //        // TODO: Add update logic here

        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        //
        // GET: /Book/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Book/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace RentalBooks.Models
{
    public class RoleViewModel
    {
        public string Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "RoleName")]
        public string Name { get; set; }
    }

    public class EditUserViewModel
    {
        public string Id { get; set; }

        [Required(AllowEmptyStrings = false)]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }
        // 1
        [Required(AllowEmptyStrings = false)]
        [Display(Name = "User Name (Log in)")]
        public string UserName { get; set; }
 
        //[Required(AllowEmptyStrings = true)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        //[Required(AllowEmptyStrings = true)]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        public IEnumerable<SelectListItem> RolesList { get; set; }
    }
}
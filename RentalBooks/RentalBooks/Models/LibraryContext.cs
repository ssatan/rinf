﻿using RentalBooks.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RentalBooks.Models
{
    public partial class LibraryContext : DbContext
    {
        //enables CRUD functionality
        public virtual DbSet<Author> Authors { get; set; }
        public virtual DbSet<Book> Books { get; set; }
        public virtual DbSet<Category> Categories { get; set; }

        public LibraryContext()
            : base("name=LibraryContext")
        {
            Database.SetInitializer(new LibraryInitializer());
        }

    }

    public partial class LibraryInitializer : DropCreateDatabaseIfModelChanges<LibraryContext>
    //public class LibraryInitializer : DropCreateDatabaseAlways<LibraryContext>
    {
        protected override void Seed(LibraryContext context)
        {
            base.Seed(context);

            var aSL = new Author { Name = "Stanislaw", Surname = "Lem", Nick = "Lem" };
            var aJT = new Author { Name = "John", Surname = "Tolkien", Nick = "Tolkien" };
            var aRC = new Author { Name = "Robyn", Surname = "Carr", Nick = "Robi" };
            var aJG = new Author { Name = "Jami", Surname = "Gerard", Nick = "Gerard" };
            var aCA = new Author { Name = "Cindy", Surname = "Alden", Nick = "Alien Girl" };
            var aSB = new Author { Name = "Stephanie", Surname = "Bond", Nick = "Bond" };
            var aKB = new Author { Name = "Kylie", Surname = "Bryant", Nick = "Bryant" };
            var aHB = new Author { Name = "Helen", Surname = "Brenna", Nick = "Brenna" };

            var cSF = new Category { CatDescription = "Science Fiction" };
            var cF = new Category { CatDescription = "Fantasy" };
            var cP = new Category { CatDescription = "Philosophy" };
            var cLA = new Category { CatDescription = "Love-affairs" };

            var bS = new Book
            {
                Title = "Solaris",
                Quantity = 3,
                Publisher = "Literary Publishing",
                ShortDescription = "Something special, rare book, the story of the thinking ocean...",
                Cover = "http://www.imdb.com/title/tt0069293/",
                Textpart =
                    "â_śWe have no need of other worlds. We need mirrors. We don't know what to do with other worlds.     A single world, our own, suffices us; but we can't accept it for what it is.â_ť\nâ_śMan has gone out     to explore other worlds and other civilizations without having explored his own labyrinth of dark     passages and secret chambers, and without finding what lies behind doorways that he himself has     sealed.â_ť"
            };
            var bP = new Book
            {
                Title = "Stories about Pilot Pirx",
                Quantity = 0,
                Publisher = "Library Newspaper 'Wyborcza'",
                ShortDescription = "The Warrior of space Pilot Pirx ...",
            };
            var bSEAL = new Book
            {
                Title = "SEAL of My Dreams",
                Quantity = 33,
                Publisher = "Amazon",
                ShortDescription =
                    " The men of the Navy SEALs are a special breed of hero, and in these stories     by eighteen top romance authors the SEALs are celebrated not only as symbols of devoted service to     their country but as the kind of man every woman wants to love.",
                Cover = "http://www.amazon.com/SEAL-My-Dreams-Stephanie-Bond-ebook/dp/B005VQVF9S",
            };
            var bHonOS = new Book
            {
                Title = "The House on Olive Street",
                Quantity = 11,
                Publisher = "Amazon",
                ShortDescription =
                    "After the death of a close friend, four women (all writers) gather at her house     for a weekend to sort through her personal papers, as well as to come to terms with what their own     lives have become. Reissue.",
                Cover = "http://www.goodreads.com/book/show/184858.The_House_on_Olive_Street",
                Textpart =
                    "The worst problems in their own lives are brought out into the open, and they find they can deal     with them because they are together. These problems range from a buried past to a dangerous marriage     to a humorous situation in which five big men are oblivious to the pressures they are placing on the     woman in their life...."
            };
            var bH = new Book
            {
                Title = "The Hobbit",
                Quantity = 12,
                Publisher = "TOLKIEN OFFICIAL ONLINE BOOK",
                ShortDescription =
                    "The Hobbit is the story of Bilbo Baggins, a hobbit who     lives in Hobbiton. He enjoys a peaceful and pastoral life but his life is interrupted by a surprise     visit by the wizard Gandalf. Before Bilbo is really able to improve upon the situation, Gandalf     has invited himself to tea and when he arrives, he comes with a company of dwarves led by Thorin.     They are embarking on a journey to recover lost treasure that is guarded by the dragon Smaug, at     the Lonely Mountain. Gandalf has decided, much to Bilbo's chagrin, that the hobbit will make an     excellent addition to the team and Bilbo is going to play the role of a burglar. As they start on     their way, Bilbo has serious misgivings.",
                Cover = "http://www.tolkien.co.uk/product/9780261103344/The+Hobbit+",
                Textpart =
                    "Bilbo Baggins is a hobbit who enjoys a comfortable, unambitious life, rarely travelling further     than the pantry of his hobbit-hole in Bag End. But his contentment is disturbed when the wizard,     Gandalf, and a company of thirteen dwarves arrive on his doorstep one day to whisk him away on an     unexpected journey â__there and back againâ_t. They have a plot to raid the treasure hoard of Smaug the     Magnificent, a large and very dangerous dragon..."
            };
            bS.Categories.Add(cSF);
            bS.Categories.Add(cF);
            bS.Authors.Add(aSL);

            bP.Categories.Add(cSF);
            bP.Authors.Add(aSL);


            bSEAL.Categories.Add(cLA);
            bSEAL.Authors.Add(aCA); bSEAL.Authors.Add(aHB); bSEAL.Authors.Add(aJG); bSEAL.Authors.Add(aKB); bSEAL.Authors.Add(aSB); bSEAL.Authors.Add(aRC);

            bHonOS.Categories.Add(cLA);

            bHonOS.Authors.Add(aJT); bHonOS.Authors.Add(aSB);

            bH.Categories.Add(cF);
            bH.Authors.Add(aJT);

            context.Books.Add(bS);
            context.Books.Add(bP);
            context.Books.Add(bSEAL);
            context.Books.Add(bHonOS);
            context.Books.Add(bH);

            context.SaveChanges();
        }

    }

}
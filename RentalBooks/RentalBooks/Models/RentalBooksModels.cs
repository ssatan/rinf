﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Web.Mvc;

namespace RentalBooks.Models
{
    public partial class Book
    {
        public Book()
        {
            Authors = new HashSet<Author>();
            Categories = new HashSet<Category>();
        }

        [HiddenInput(DisplayValue = false)]
        public int BookID { get; set; }

        [Required(ErrorMessage = "You must imput title")]
        [StringLength(200)]
        public string Title { get; set; }

        [Range(0, 9999)]
        public int? Quantity { get; set; }

        [StringLength(100)]
        public string Publisher { get; set; }

        [DataType(DataType.MultilineText)]
        [Display(Description = "Short Description")]
        [StringLength(1000)]
        public string ShortDescription { get; set; }

        [Display(Description="Link to the picture of the cover")]
        [StringLength(200)]
        public string Cover { get; set; }

        [Display(Description = "The best pieces ...")]
        [StringLength(3000)]
        public string Textpart { get; set; }

        public virtual ICollection<Author> Authors { get; set; }

        public virtual ICollection<Category> Categories { get; set; }
    }

    public partial class Author
    {
        public Author()
        {
            Books = new HashSet<Book>();
        }

        public int AuthorID { get; set; }

        [Required]
        [StringLength(30)]
        public string Name { get; set; }

        [Required]
        [StringLength(60)]
        public string Surname { get; set; }

        [StringLength(30)]
        public string Nick { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }

    public partial class Category
    {
        public Category()
        {
            Books = new HashSet<Book>();
        }

        [Key]
        public int CatID { get; set; }

        [Required]
        [StringLength(30)]
        public string CatDescription { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }

    public partial class AuthorBis
    {
        public bool SwitchOn { get; set;}
        public string Name { get; set; }
        public string Surname { get; set; }
        public int AuthorID { get; set; }
    }


}
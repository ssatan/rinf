﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RentalBooks.Models;

namespace RentalBooks.Models
{
    public class PagingInfo
    {
        public int TotalItems { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }

        public int TotalPages
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage); }
        }
    }
    public class BooksListViewModel
    {
        public IEnumerable<Book> books { get; set; }
        public PagingInfo pagingInfo { get; set; }
    }
    public class CartIndexViewModel
    {
        public Cart Cart { get; set; }
        public string ReturnUrl { get; set; }
    }
    public class AuthorsIndexViewModel
    {
        public IEnumerable<Book> books { get; set; }
        public IEnumerable<Author> authors { get; set; }
        public int book_id;
        public string ReturnUrl { get; set; }
    }
    public class ItemViewModel
    {
        public ItemViewModel() { }
        public Book book;
        public Author author;
        public string returnUrl;
        public List<AuthorBis> authorsBis;
        public ItemViewModel(IEnumerable<Author> authors)
        {
            if (book != null)
            {
                authorsBis = new List<AuthorBis>();
                Author b=null;
                foreach (Author a in authors)
                {
                    
                    b=book.Authors.First(x => x.AuthorID == a.AuthorID);
                    AuthorBis ab= new AuthorBis{ AuthorID=a.AuthorID, Name=a.Name, Surname=a.Surname };
                    if (b == null) ab.SwitchOn = false; else ab.SwitchOn = true;
                    authorsBis.Add(ab);
                }
            }
        }
    }
}
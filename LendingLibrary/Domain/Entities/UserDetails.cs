﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Domain.Entities
{
    public class UserDetails
    {
        [Required(ErrorMessage = "ProszÄt podaÄ┼ nazwisko.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "ProszÄt podaÄ┼ pierwszy wiersz adres.")]
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string Line3 { get; set; }

        [Required(ErrorMessage = "ProszÄt podaÄ┼ nazwÄt miasta.")]
        public string City { get; set; }

        [Required(ErrorMessage = "ProszÄt podaÄ┼ nazwÄt wojewĂłdztwa.")]
        public string State { get; set; }

        public string Zip { get; set; }

        [Required(ErrorMessage = "ProszÄt podaÄ┼ nazwÄt kraju.")]
        public string Country { get; set; }

        public bool GiftWrap { get; set; }

    }
}

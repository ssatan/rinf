﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;

namespace Domain.Entities
{
    public class Book
    {
        public int BookID { get; set; }
        public string Title { get; set; }
        public string Publisher { get; set; }
        public string ShortDiscription { get; set; }
        public string TextPart { get; set; }
        public int Quantity { get; set; }
        public List<Category> categories { get; set; }
        public List<Author> authors { get; set; }
        public string Cover { get; set; }
    }
    public class Author
    {
        public int AuthorID { get; set; }
        public string Nick { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
    public class writers
    {
        public int WriterID { get; set; }
        public int BookID { get; set; }
        public int AuthorID { get; set; }
    }
    public class Category
    {
        public int CatID { get; set;}
        public string CatDescriprion { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Domain.Abstract;
using Domain.Entities;
using WebUI.HtmlHelpers;
using WebUI.Models;

namespace WebUI.Controllers
{
    public class BookController : Controller
    {
        private IBookRepository repo;
        public int PageSize = 2;

        public BookController(IBookRepository bookRepo)
        {
            repo = bookRepo;
        }
        
        public ViewResult List(int page=1)
        {
            ViewBag.Title = "Lend a Book ...";
            BooksListViewModel viewModel = new BooksListViewModel
            {
                books = repo.books.OrderBy(b => b.BookID).Skip((page - 1) * PageSize).Take(PageSize),
                pagingInfo = new PagingInfo { CurrentPage = page, ItemsPerPage = PageSize, TotalItems = repo.books.Count() }
            };
            return View(viewModel);
            //ViewBag.pageInfo = new PagingInfo { CurrentPage = page, TotalItems = repo.books.Count(), ItemsPerPage = PageSize };
            //return View(repo.books.OrderBy( b => b.BookID).Skip((page - 1) * PageSize).Take(PageSize));
        }

    }
}

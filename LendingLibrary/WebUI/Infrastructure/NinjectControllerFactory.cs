﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Optimization;
using System.Web.Mvc;
using System.Web.Routing;
using Ninject;
using Domain.Entities;
using Domain.Abstract;
using Moq;


namespace WebUI.Infrastructure
{
    public class MyList<T> : List<T>
    {
        public MyList(params T[] l) : base() { for (int i = 0; i < l.Length; i++) this.Add(l[i]); }
    }
    public class NinjectControllerFactory : DefaultControllerFactory
    {
        private IKernel ninjectKernel;
        public NinjectControllerFactory()
        {

            ninjectKernel = new StandardKernel();
            AddBindings();
        }

        private List<Book> fake
        { 
            get 
            {
                
                Category cSF= new Category { CatID = 1, CatDescriprion = "Science Fiction" };
                Category cF = new Category { CatID = 2, CatDescriprion = "Fantasy" };
                Category cPh = new Category { CatID = 3, CatDescriprion = "Philosophy" };
                Category cLv = new Category { CatID =4, CatDescriprion="love-affairs"};
                Author aSL = new Author { AuthorID = 1, Name = "Stanisław", Surname = "Lem", Nick = "Lem" };
                Author aJT = new Author { AuthorID = 2, Name = "John", Surname = "Tolkien", Nick = "Tolkien" };
                Author aRC = new Author {AuthorID = 3, Name = "Robyn" , Nick="Robi", Surname = "Carr" };
                //Author aCG = new Author {AuthorID = 4, Name = "Jami" , Nick="Gerard", Surname = "Gerard" };
                //Author aCA = new Author {AuthorID = 5, Name = "Cindy" , Nick="Alden", Surname = "Alden" };
                //Author aSB = new Author {AuthorID = 6, Name = "Stephanie" , Nick="Bond", Surname = "Bond" };
                //Author aKB = new Author {AuthorID = 7, Name = "Kylie" , Nick="Bryant", Surname = "Bryant" };
                //Author aHB = new Author {AuthorID = 8, Name = "Helen" , Nick="Brenna", Surname = "Brenna" };
                List<Book> books = new List<Book>();
                books.Add(new Book { BookID = 1, Quantity = 3, Publisher = "Literary Publishing", ShortDiscription = "Something special, rare book, the story of the thinking ocean...", Title = "Solaris", categories = new MyList<Category>(cPh, cSF), authors = new MyList<Author>(aSL), Cover = "http://www.imdb.com/title/tt0069293/", TextPart = "“We have no need of other worlds. We need mirrors. We don't know what to do with other worlds. A single world, our own, suffices us; but we can't accept it for what it is.”\n“Man has gone out to explore other worlds and other civilizations without having explored his own labyrinth of dark passages and secret chambers, and without finding what lies behind doorways that he himself has sealed.”" });
                books.Add(new Book { BookID = 2, Quantity = 0, Publisher = "Library Newspaper \"Wyborcza\"", ShortDiscription = "The Warrior of space Pilot Pirx ...", Title = "Stories about Pilot Pirx", categories = new MyList<Category>(cSF), authors = new MyList<Author>(aSL) });
                books.Add(new Book { BookID = 3, Quantity = 33, Publisher = "Amazon", ShortDiscription = " the men of the Navy SEALs are a special breed of hero, and in these stories by eighteen top romance authors the SEALs are celebrated not only as symbols of devoted service to their country but as the kind of man every woman wants to love.", Title = "SEAL of My Dreams", categories = new MyList<Category>(cLv), Cover="http://www.amazon.com/SEAL-My-Dreams-Stephanie-Bond-ebook/dp/B005VQVF9S" });
                books[2].authors = new List<Author>()
                {
                    new Author {AuthorID = 3, Name = "Robyn" , Nick="Robi", Surname = "Carr" },
                    new Author {AuthorID = 4, Name = "Jami" , Nick="Gerard", Surname = "Gerard" },
                    new Author {AuthorID = 5, Name = "Cindy" , Nick="Alden", Surname = "Alden" },
                    new Author {AuthorID = 6, Name = "Stephanie" , Nick="Bond", Surname = "Bond" },
                    new Author {AuthorID = 7, Name = "Kylie" , Nick="Bryant", Surname = "Bryant" },
                    new Author {AuthorID = 8, Name = "Helen" , Nick="Brenna", Surname = "Brenna" },
                };
                books.Add(new Book { BookID = 4, categories = new MyList<Category>(cLv), Publisher = "Amazon", authors = new MyList<Author>(aRC), Cover = "http://www.goodreads.com/book/show/184858.The_House_on_Olive_Street", Quantity = 11, ShortDiscription = "After the death of a close friend, four women (all writers) gather at her house for a weekend to sort through her personal papers, as well as to come to terms with what their own lives have become. Reissue.", TextPart = "The worst problems in their own lives are brought out into the open, and they find they can deal with them because they are together. These problems range from a buried past to a dangerous marriage to a humorous situation in which five big men are oblivious to the pressures they are placing on the woman in their life....", Title = "The House on Olive Street" });
                books.Add(new Book { BookID = 5, categories = new MyList<Category>(cF), authors = new MyList<Author>(aJT), Title = "The Hobbit", Quantity = 12, Publisher = "TOLKIEN OFFICIAL ONLINE BOOK", Cover = "http://www.tolkien.co.uk/product/9780261103344/The+Hobbit+", TextPart = "Bilbo Baggins is a hobbit who enjoys a comfortable, unambitious life, rarely travelling further than the pantry of his hobbit-hole in Bag End. But his contentment is disturbed when the wizard, Gandalf, and a company of thirteen dwarves arrive on his doorstep one day to whisk him away on an unexpected journey ‘there and back again’. They have a plot to raid the treasure hoard of Smaug the Magnificent, a large and very dangerous dragon…", ShortDiscription = @"The Hobbit is the story of Bilbo Baggins, a hobbit who lives in Hobbiton. He enjoys a peaceful and pastoral life but his life is interrupted by a surprise visit by the wizard Gandalf. Before Bilbo is really able to improve upon the situation, Gandalf has invited himself to tea and when he arrives, he comes with a company of dwarves led by Thorin. They are embarking on a journey to recover lost treasure that is guarded by the dragon Smaug, at the Lonely Mountain. Gandalf has decided, much to Bilbo's chagrin, that the hobbit will make an excellent addition to the team and Bilbo is going to play the role of a burglar. As they start on their way, Bilbo has serious misgivings. " });
                return books;
            } 
        }

        protected override IController GetControllerInstance(RequestContext 
            requestContext, Type controllerType) {

            return controllerType == null
                ? null
                : (IController)ninjectKernel.Get(controllerType);
        }

        private void AddBindings()
        {
            Mock<IBookRepository> mock = new Mock<IBookRepository>();
            //mock.Setup(m => m.books).Returns(
            //    new List<Book>() {
            //    new Book { BookID = 1, Quantity = 3, Publisher = "Literary Publishing", ShortDiscription = "Something special, rare book, the story of the thinking ocean...", Title = "Solaris" } }.AsQueryable());
            mock.Setup(x => x.books).Returns(fake.AsQueryable());
            ninjectKernel.Bind<IBookRepository>().ToConstant(mock.Object);
        }
    }
}
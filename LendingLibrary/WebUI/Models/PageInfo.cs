﻿using Domain.Entities;
using System;
using System.Linq;

    namespace WebUI.Models
    {

        public class PagingInfo
        {
            public int TotalItems { get; set; }
            public int ItemsPerPage { get; set; }
            public int CurrentPage { get; set; }

            public int TotalPages
            {
                get { return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage); }
            }
        }
        public class BooksListViewModel
        {
            public IQueryable<Book> books { get; set; }
            public PagingInfo pagingInfo { get; set; }
        }
    }
